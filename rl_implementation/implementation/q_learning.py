from multiprocessing.pool import Pool

import gym
import sys
import os
import random
import time

from rl_implementation.hyperparameter_tuning import quick_policy_check
import matplotlib.pyplot as plt
import numpy as np

plt.style.use('ggplot')

class QLearning:
    # q_values_initializer: int or None for random values
    def __init__(self, environment_name, q_table_input_file, q_table_output_file, alpha=0.1, epsilon=0.1,
                 epsilon_discount=0.999, gamma=0.9,
                 q_table_initializer=None, logger=None, metric_output_folder='./plots'):

        self.old_q_table = None

        self.q_table_input_file = q_table_input_file
        self.q_table_output_file = q_table_output_file
        self.alpha = alpha
        self.epsilon = epsilon
        self.epsilon_discount = epsilon_discount
        self.gamma = gamma
        self.metric_output_folder = metric_output_folder

        self.environment = gym.make(environment_name)

        self.average_reward_per_episode = []
        self.time_steps_per_episode = []
        self.cumulative_reward_per_episode = []
        self.epsilon_value_per_episode = []
        self.q_value_error_sum = []

        if self.q_table_input_file and os.path.isfile(self.q_table_input_file):
            self.q_table = np.genfromtxt(self.q_table_input_file, delimiter=',')
            # print("Load q_table of " + self.q_table_input_file + " successfully ")
            # print(self.q_table)
            if self.q_table.shape != (self.environment.observation_space.n, self.environment.action_space.n):
                self.logger.error('Dimensions of given q-table do not match.')
                sys.exit(1)
        else:
            if q_table_initializer is None:
                self.q_table = np.random.rand(self.environment.observation_space.n, self.environment.action_space.n)
            else:
                try:
                    self.q_table = np.full(
                        (self.environment.observation_space.n, self.environment.action_space.n),
                        float(q_table_initializer)
                    )
                except ValueError:
                    self.logger.error('Initial q-values have to be a number.')
                    sys.exit(1)

    def train(self, max_number_of_episodes=100000):

        convergence = False
        episode_counter = 0
        while not convergence:

            time_steps = self._episode()

            # epsilon discount
            self.epsilon *= self.epsilon_discount

            convergence = self._check_for_convergence() or episode_counter >= max_number_of_episodes

            episode_counter += 1

        # print('Finished training after episode %i.' % (episode_counter))
        self._print_metric()
        self._persist_q_table()

        return episode_counter

    def play(self, number_of_games=1, delay_between_moves=0.2, output=True, to_buffer=False):
        self.epsilon = 0
        episode_buffer = []

        for i in range(number_of_games):
            _, current_episode_frames = self._episode(output, delay_between_moves, to_buffer)
            episode_buffer.append(current_episode_frames)
            if output:
                time.sleep(1)
                os.system('clear')

        return episode_buffer if to_buffer else None

    def _episode(self):
        self.old_q_table = np.copy(self.q_table)
        rewards = []
        self.current_state = self.environment.reset()

        done = False
        time_steps = 0
        while not done:
            time_steps += 1

            # explore or exploit
            if random.uniform(0, 1) < self.epsilon:
                self.current_action = self.environment.action_space.sample()  # explore
            else:
                # exploitation
                action = np.argmax(self.q_table[self.current_state])

                # ties broken equally if more than one action satisfies condition
                all_max_indices = \
                    np.where(self.q_table[self.current_state] == self.q_table[self.current_state][action])[0]
                self.current_action = np.random.choice(all_max_indices)

            next_state, reward, done, info = self.environment.step(self.current_action)

            rewards.append(reward)

            old_q_value = self.q_table[self.current_state, self.current_action]

            next_max = np.max(self.q_table[next_state]) if not done else 0.0

            new_q_value = old_q_value + self.alpha * (reward + self.gamma * next_max - old_q_value)

            self.q_table[self.current_state, self.current_action] = new_q_value
            self.current_state = next_state

        # save metric data from episode
        self.cumulative_reward_per_episode.append(sum(rewards))
        self.average_reward_per_episode.append(sum(rewards) / time_steps)
        self.time_steps_per_episode.append(time_steps)
        self.epsilon_value_per_episode.append(self.epsilon)

        return time_steps

    def convergence_metric(self, max_number_of_episodes=20000):
        convergence_data = []

        convergence = False
        episode_counter = 0
        while not convergence:
            time_steps = self._episode()

            # epsilon discount
            self.epsilon *= self.epsilon_discount

            # if episode_counter % 1000 == 0:
            #     self._persist_q_table()
            #     # print('Run episode %i, finished after %i time steps.' % (episode_counter, time_steps))

            # if episode_counter % 1000 == 0:
            # quick_policy_check.quick_policy_check_q_table(self.q_table)
            convergence = self._check_for_convergence() or episode_counter >= max_number_of_episodes

            if not convergence:
                perfect_actions_propotion = quick_policy_check.quick_policy_check_q_table(self.q_table)
                convergence_data.append(perfect_actions_propotion)

            # if episode_counter % 1000 == 0:
            # print(quick_policy_check.quick_policy_check_q_table(self.q_table))

            episode_counter += 1

        # print('Finished training after episode %i.' % (episode_counter))

        string_of_eps = str(round(self.epsilon, 2))
        string_of_alph = str(round(self.alpha, 2))

        with open('./convergence_data_lists/q_learning_' + string_of_eps + '_' + string_of_alph, 'w') as f:
            for item in convergence_data:
                f.write("%s\n" % item)


    def _check_for_convergence(self):
        return quick_policy_check.quick_policy_check_q_table(self.q_table, False) >= 0.95


    def _persist_q_table(self):
        if not self.q_table_output_file:
            return

        if not os.path.exists(os.path.dirname(self.q_table_output_file)):
            os.makedirs(os.path.dirname(self.q_table_output_file))

        if os.path.isfile(self.q_table_output_file):
            os.remove(self.q_table_output_file)

        np.savetxt(self.q_table_output_file, self.q_table, delimiter=',')


def evaluate_alpha(alpha):
    for epsilon in np.arange(1.0, 0.7, -0.1):
        mean = 0
        for mean_counter in range(10):
            q = QLearning('Taxi-v2', None, None,
                          epsilon_discount=epsilon_decay,
                          q_table_initializer=0, epsilon=epsilon, alpha=alpha, gamma=gamma)

            mean += q.train()

        mean = mean / 10
        print("###Finisher after %i for alpha=%.2f epsilon=%.2f epsilon_decay=%.5f gamma=%.2f."
              % (mean, alpha, epsilon, epsilon_decay, gamma))


if __name__ == '__main__':
    epsilon_decay = 1.  # .9995
    gamma = 1  # .75

    p = Pool()
    task_results = p.map(evaluate_alpha, np.arange(1.0, 0.0, -0.1))
    p.close()
