import gym
import sys
import os
import random

from rl_implementation.hyperparameter_tuning import quick_policy_check
import matplotlib.pyplot as plt
import numpy as np

plt.style.use('ggplot')

'''
To Do: 
- Metrik zum Vergleich vom Einfluss der Umweltwahrscheinlichkeiten auf die Performanz (Vergleich von QLearning und SARSA)
- Q-learning with probability metric: One point -> How many times in 10 failed
- Trennen von Eingabe- und Ausgabe Dateiens
- change epsilon to log function
- Funktion zum registrieren von enviroments
'''


class SARSA:
    # q_values_initializer: int or None for random values
    def __init__(self, environment_name, q_table_input_file, q_table_output_file, alpha=0.1, epsilon=0.1,
                 epsilon_discount=0.999, gamma=0.9,
                 q_table_initializer=None, logger=None, metric_output_folder='./plots'):

        self.old_q_table = None

        self.q_table_input_file = q_table_input_file
        self.q_table_output_file = q_table_output_file
        self.alpha = alpha
        self.epsilon = epsilon
        self.epsilon_discount = epsilon_discount
        self.gamma = gamma
        self.metric_output_folder = metric_output_folder

        self.environment = gym.make(environment_name)
        self.current_state = None
        self.current_action = None

        self.average_reward_per_episode = []
        self.time_steps_per_episode = []
        self.cumulative_reward_per_episode = []
        self.epsilon_value_per_episode = []
        self.q_value_error_sum = []

        if self.q_table_input_file and os.path.isfile(self.q_table_input_file):
            self.q_table = np.genfromtxt(self.q_table_input_file, delimiter=',')
            # print("Load q_table of " + self.q_table_input_file + " successfully ")
            # print(self.q_table)
            if self.q_table.shape != (self.environment.observation_space.n, self.environment.action_space.n):
                self.logger.error('Dimensions of given q-table do not match.')
                sys.exit(1)
        else:
            if q_table_initializer is None:
                self.q_table = np.random.rand(self.environment.observation_space.n, self.environment.action_space.n)
            else:
                try:
                    self.q_table = np.full(
                        (self.environment.observation_space.n, self.environment.action_space.n),
                        float(q_table_initializer)
                    )
                except ValueError:
                    self.logger.error('Initial q-values have to be a number.')
                    sys.exit(1)

    def train(self, max_number_of_episodes=10000):
        convergence = False
        episode_counter = 0
        while not convergence:
            time_steps = self._episode()

            # epsilon discount
            self.epsilon *= self.epsilon_discount

            # if episode_counter % 1000 == 0:
            #     self._persist_q_table()
            #     # print('Run episode %i, finished after %i time steps.' % (episode_counter, time_steps))

            # if episode_counter % 1000 == 0:
            # quick_policy_check.quick_policy_check_q_table(self.q_table)
            convergence = self._check_for_convergence() or episode_counter >= max_number_of_episodes

            # if episode_counter % 1000 == 0:
            # print(quick_policy_check.quick_policy_check_q_table(self.q_table))

            episode_counter += 1

        # print('Finished training after episode %i.' % (episode_counter))

        return episode_counter

    def convergence_metric(self, max_number_of_episodes=20000):
        convergence_data = []

        convergence = False
        episode_counter = 0
        while not convergence:
            time_steps = self._episode()

            # epsilon discount
            self.epsilon *= self.epsilon_discount

            # if episode_counter % 1000 == 0:
            #     self._persist_q_table()
            #     # print('Run episode %i, finished after %i time steps.' % (episode_counter, time_steps))

            # if episode_counter % 1000 == 0:
            # quick_policy_check.quick_policy_check_q_table(self.q_table)
            convergence = self._check_for_convergence() or episode_counter >= max_number_of_episodes

            if not convergence:
                perfect_actions_propotion = quick_policy_check.quick_policy_check_q_table(self.q_table)
                convergence_data.append(perfect_actions_propotion)

            # if episode_counter % 1000 == 0:
            # print(quick_policy_check.quick_policy_check_q_table(self.q_table))

            episode_counter += 1

        # print('Finished training after episode %i.' % (episode_counter))

        string_of_eps = str(round(self.epsilon, 2))
        string_of_alph = str(round(self.alpha, 2))

        with open('./convergence_data_lists/sarsa_' + string_of_eps + '_' + string_of_alph, 'w') as f:
            for item in convergence_data:
                f.write("%s\n" % item)

    def _episode(self):
        rewards = []
        self.current_state = self.environment.reset()

        done = False
        time_steps = 0
        while not done:
            time_steps += 1

            if self.current_action is None:
                if random.uniform(0, 1) < self.epsilon:
                    # exploration
                    self.current_action = self.environment.action_space.sample()
                else:
                    # exploitation
                    action = np.argmax(self.q_table[self.current_state])

                    # ties broken equally if more than one action satisfies condition
                    all_max_indices = \
                        np.where(self.q_table[self.current_state] == self.q_table[self.current_state][action])[0]
                    self.current_action = np.random.choice(all_max_indices)

            next_state, reward, done, info = self.environment.step(self.current_action)
            rewards.append(reward)

            if done and reward != 20:
                break

            # explore or exploit
            if random.uniform(0, 1) < self.epsilon:
                # exploration
                new_action = self.environment.action_space.sample()
            else:
                # exploitation
                action = np.argmax(self.q_table[next_state])

                # ties broken equally if more than one action satisfies condition
                all_max_indices = \
                    np.where(self.q_table[next_state] == self.q_table[next_state][action])[0]
                new_action = np.random.choice(all_max_indices)

            # Q(S_t, A_t)
            current_q: float = self.q_table[self.current_state, self.current_action]
            next_q: float = self.q_table[next_state, new_action] if reward != 20 else 0.0

            self.q_table[self.current_state, self.current_action] \
                = current_q + self.alpha * (
                    reward + self.gamma * next_q - current_q)

            self.current_state = next_state if not done else None
            self.current_action = new_action if not done else None

        # save metric data from episode
        self.cumulative_reward_per_episode.append(sum(rewards))
        self.average_reward_per_episode.append(sum(rewards) / time_steps)
        self.time_steps_per_episode.append(time_steps)

        return time_steps

    def _check_for_convergence(self):
        return quick_policy_check.quick_policy_check_q_table(self.q_table, False) >= 0.95

    def _print_metric(self):
        if not os.path.isdir(self.metric_output_folder):
            os.makedirs(self.metric_output_folder)

        plt.title('Time steps per episode')
        plt.xlabel('Episode')
        plt.ylabel('Time steps')
        # plt.scatter(range(1, len(self.time_steps_per_episode) + 1), self.time_steps_per_episode, s=0.5)
        # plt.savefig('./plots/time_steps_per_episode.png')
        # plt.show()

        plt.title('Cumulative reward per episode')
        plt.xlabel('Episode')
        plt.ylabel('Cumulative reward')
        plt.scatter(range(1, len(self.cumulative_reward_per_episode) + 1), self.cumulative_reward_per_episode, s=0.5)
        # plt.plot(range(1, len(self.cumulative_reward_per_episode) + 1), self.cumulative_reward_per_episode, linewidth=0.2)
        # plt.savefig('./plots/cumulative_reward_per_episode.png')
        # plt.show()

        # plt.title('Log. Regression of cumulative reward per episode')
        plt.xlabel('Episode')
        plt.ylabel('Cumulative reward per episode')
        # plt.axhline(np.average(self.cumulative_reward_per_episode[1000:]), color="red")
        # plt.savefig('./plots/log_regression_of_cumulative_reward_per_episode.png', dpi=300)
        # plt.show()

        plt.title('Average reward per episode')
        plt.xlabel('Episode')
        plt.ylabel('Average reward')
        # plt.scatter(range(1, len(self.average_reward_per_episode) + 1), self.average_reward_per_episode, s=0.5)
        # plt.savefig('./plots/average_reward_per_episode.png')
        # plt.show()

        plt.title('Q table error')
        plt.xlabel('Episode')
        plt.ylabel('q error')
        # plt.scatter(range(1, len(self.q_value_error_sum) + 1), self.q_value_error_sum, s=0.5)
        # plt.savefig('./plots/q_table_error.png')
        # plt.show()

        plt.title('Epsilon per episode')
        plt.xlabel('Episode')
        plt.ylabel('Epsilon')
        # plt.scatter(range(1, len(self.epsilon_value_per_episode) + 1),

    #                    self.epsilon_value_per_episode, s=0.5)
    # plt.savefig('./plots/epsilon_per_episode.png')
    # plt.show()

    def _persist_q_table(self):
        if not self.q_table_output_file:
            return

        if not os.path.exists(os.path.dirname(self.q_table_output_file)):
            os.makedirs(os.path.dirname(self.q_table_output_file))

        if os.path.isfile(self.q_table_output_file):
            os.remove(self.q_table_output_file)

        np.savetxt(self.q_table_output_file, self.q_table, delimiter=',')


def evaluate_alpha(alpha):
    for epsilon in np.arange(.6, 0.0, -0.1):
        mean = 0
        for mean_counter in range(10):
            q = SARSA('Taxi-v2', None, None,
                      epsilon_discount=epsilon_decay,
                      q_table_initializer=0, epsilon=epsilon, alpha=alpha, gamma=gamma)

            mean += q.train()

        mean = mean / 10
        print("###Finisher after %i for alpha=%.2f epsilon=%.2f epsilon_decay=%.5f gamma=%.2f."
              % (mean, alpha, epsilon, epsilon_decay, gamma))


if __name__ == '__main__':
    epsilon_decay = 1.  # .9995
    gamma = 1  # .75

    #####Convergence
    epsilon = .9
    alpha = .5
    q = SARSA('Taxi-v2', None, None,
              epsilon_discount=epsilon_decay,
              q_table_initializer=0, epsilon=epsilon, alpha=alpha, gamma=gamma)

    q.convergence_metric()

    #######

    #p = Pool()
    #task_results = p.map(evaluate_alpha, np.arange(1.0, 0.0, -0.1))
    #p.close()

    # q.play(1)
