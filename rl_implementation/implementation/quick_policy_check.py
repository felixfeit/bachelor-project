import numpy as np

q_table_optimal = np.genfromtxt('./q_tables/optimal_taxi_q_table', delimiter=',')

def _optimal_policy(state):
    actions = list(q_table_optimal[state])
    actions.sort()
    actions = list(reversed(actions))
    best_actions = actions[:2]

    first_best_action = best_actions[0]
    second_best_action = best_actions[1]

    # choose actions
    actions = list(q_table_optimal[state])
    if first_best_action == second_best_action:
        return [i for i, x in enumerate(actions) if x == first_best_action]

    else:
        return [actions.index(first_best_action)]


def quick_policy_check_q_table(q_table, output=True):
    number_of_right_actions = 0
    for state_index in range(500):
        if np.argmax(q_table[state_index]) in _optimal_policy(state_index):
            number_of_right_actions += 1

    if output:
        print("Right values: %.2f%%" % (number_of_right_actions * 100 / 500))

    return number_of_right_actions / 500
