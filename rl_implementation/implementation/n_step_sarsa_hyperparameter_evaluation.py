from operator import itemgetter
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from numpy.random import randn
from scipy import array, newaxis

data = [(0.25, 0.05, 10001.0), (0.25, 0.1, 10001.0), (0.25, 0.15, 10001.0), (0.3, 0.05, 8354.5), (0.3, 0.1, 9975.6), (0.3, 0.15, 10001.0), (0.35, 0.05, 4421.3), (0.35, 0.1, 9538.0), (0.35, 0.15, 10001.0), (0.4, 0.05, 2775.1), (0.4, 0.1, 7533.4), (0.4, 0.15, 10001.0), (0.45, 0.05, 2852.6), (0.45, 0.1, 5110.6), (0.45, 0.15, 10001.0), (0.5, 0.05, 3041.3), (0.5, 0.1, 3701.6), (0.5, 0.15, 10001.0), (0.55, 0.05, 3626.2), (0.55, 0.1, 5287.7), (0.55, 0.15, 9935.4), (0.6000000000000001, 0.05, 4062.2), (0.6000000000000001, 0.1, 4928.6), (0.6000000000000001, 0.15, 10001.0), (0.6500000000000001, 0.05, 5390.6), (0.6500000000000001, 0.1, 7962.0), (0.6500000000000001, 0.15, 10001.0), (0.7000000000000002, 0.05, 6657.6), (0.7000000000000002, 0.1, 8451.3), (0.7000000000000002, 0.15, 10001.0), (0.7500000000000002, 0.05, 7620.2), (0.7500000000000002, 0.1, 10001.0), (0.7500000000000002, 0.15, 10001.0)]

data = np.array(data)

X = data[:, 0]
Y = data[:, 1]
Z = data[:, 2]

fig = plt.figure()
fig.set_dpi(200)
ax = fig.add_subplot(111, projection='3d')
surf = ax.plot_trisurf(X, Y, Z, cmap=cm.RdBu, linewidth=.5, antialiased=True, edgecolors='w')

ax.set_xlabel('Epsilon')
ax.set_ylabel('Alpha')

ax.zaxis.set_rotate_label(False)
ax.zaxis.label.set_rotation(90)
ax.set_zlabel('Anzahl an Episoden')

ax.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
ax.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
ax.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))

for spine in ax.spines.values():
    spine.set_visible(False)

fig.tight_layout()

ax.view_init(azim=250)
plt.savefig('n_step_hpt.svg', format='svg', dpi=600, bbox_inches="tight")
