from operator import itemgetter

import matplotlib

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from numpy.random import randn
from scipy import array, newaxis

data = []

max_number_of_episodes = 10000

with open("tuning_results_sarsa", "r") as ins:
    index_counter = 0
    for line in ins:
        if line == '\n':
            continue

        number_of_episodes = int(line.rsplit(' for alpha=')[0].split(' ')[-1])
        alpha = float(line.rsplit(' epsilon=')[0].split('=')[-1])
        epsilon = float(line.rsplit(' epsilon_decay=')[0].split('=')[-1])

        if number_of_episodes > max_number_of_episodes:
            number_of_episodes = max_number_of_episodes

        if number_of_episodes < 3000:
            print((epsilon, alpha, number_of_episodes))

        if alpha <= 0.6:
            data.append((epsilon, alpha, number_of_episodes))

        index_counter += 1

data = np.array(data)

# Make data.
X = data[:, 0]
Y = data[:, 1]
Z = data[:, 2]

fig = plt.figure()

ax = fig.add_subplot(111, projection='3d')
surf = ax.plot_trisurf(X, Y, Z, cmap=cm.RdBu, linewidth=.5, antialiased=True, edgecolors='w')

ax.set_xlabel('Epsilon')
ax.set_ylabel('Alpha')
ax.zaxis.set_rotate_label(False)
ax.zaxis.label.set_rotation(90)
ax.set_zlabel('Anzahl an Episoden')
ax.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
ax.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
ax.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))

for spine in ax.spines.values():
    spine.set_visible(False)

fig.tight_layout()

ax.view_init(azim=220)
plt.savefig('sarsa_hpt.svg', format='svg', dpi=600, bbox_inches="tight")
