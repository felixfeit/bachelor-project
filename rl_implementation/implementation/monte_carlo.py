import random
import os
import gym
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import FuncFormatter

from rl_implementation.hyperparameter_tuning.quick_policy_check import quick_policy_check_q_table

mc_data_10_repetitions = {
    '0.05': 41111.9,
    '0.10': 12466.2,
    '0.15': 5234.7,
    '0.20': 3665.7,
    '0.25': 2221.2,
    '0.30': 1906.2,
    '0.35': 1431.3,
    '0.40': 1398.3,
    '0.45': 1139.16,
    '0.50': 988.8,
    '0.55': 980.04,
    '0.60': 850.92,
    '0.65': 877.98,
    '0.70': 884.4,
    '0.75': 886.02,
    '0.80': 1078.5,
    '0.85': 1470.54,
    '0.90': 3138.0,
    '0.95': 25040.7
}

mc_data_100_repetitions = \
    {'0.25': 2454.78, '0.3': 1700.46, '0.35': 1551.27, '0.4': 1248.36, '0.45': 1134.21, '0.5': 1025.64, '0.55': 946.86,
     '0.6': 899.55, '0.65': 830.88, '0.7': 858.54, '0.75': 907.02, '0.8': 1021.71, '0.85': 1441.35}

# mc_data_10_reps_95 = {'0.5': 70000.0, '0.55': 65947.3, '0.6': 50827.1, '0.65': 32815.2,
#                       '0.7': 17796.3, '0.75': 11429.1, '0.8': 9075.9, '0.85': 10803.6, '0.86': 13744.5, '0.87': 18295.5,
#                       '0.88': 23359.5}

mc_data_10_reps_95 = {'0.5': 70000.0, '0.55': 65947.3, '0.6': 50827.1, '0.65': 32815.2,
                      '0.7': 17796.3, '0.75': 11429.1, '0.8': 9075.9, '0.85': 10803.6,
                      '0.88': 23359.5}

x = [float(key) for key, _ in mc_data_10_reps_95.items()][3:]
y = [value for _, value in mc_data_10_reps_95.items()][3:]

_, ax1 = plt.subplots()

ax1.set_xlabel('Epsilon')
ax1.set_ylabel('Anzahl an Episoden')

quadratic_function_params = np.polyfit(x[1:-1], y[1:-1], 2)
quadratic_function = np.poly1d(quadratic_function_params)

ax1.plot(np.arange(x[0], x[-1] + 0.01, 0.01), quadratic_function(np.arange(x[0], x[-1] + 0.01, 0.01)), color='darkred')

ax1.scatter(x, y, s=10)
plt.savefig('mc_epsilon_evaluation_new.svg', format='svg', dpi=600, bbox_inches="tight")


input()



# implementation
def evaluate_epsilon_monte_carlo(epsilon, required_perfect_actions_propotion=0.95, gamma=1):
    env = gym.make("Taxi-v2")  # Create environment
    action_size = env.action_space.n  # Number of possible actions
    state_size = env.observation_space.n  # Number of possible states


    q_table = np.zeros((state_size, action_size))
    # q_table = np.genfromtxt('./optimal_taxi_q_table', delimiter=',')

    # incremental: store tuples with current avg and number of values (2D-array -> 500x6)
    number_of_visits = np.zeros((state_size, action_size))

    episode = 0
    right_actions_proptions = []
    while True:
        # generate full episode
        episode_steps = []
        state = env.reset()  # Reset the environment
        done = False

        while not done:
            if random.uniform(0, 1) < epsilon:
                # exploration
                action = env.action_space.sample()
            else:
                # exploitation
                action = np.argmax(q_table[state])

                # ties broken equally if more than one action satisfies condition
                all_max_indices = np.where(q_table[state] == q_table[state][action])[0]
                action = np.random.choice(all_max_indices)

            # execute action
            new_state, reward, done, info = env.step(action)

            # if done:
            #     print(reward)

            # (S_t, A_t, R_{t+1})
            episode_steps.append((state, action, reward))

            state = new_state

        # if len(episode_steps) == env._max_episode_steps:
        #     continue

        # calculate new averages and adjust policy
        G = 0

        for t in reversed(range(len(episode_steps))):
            S_t, A_t, R_t_plus_1 = episode_steps[t]
            G = gamma * G + R_t_plus_1

            # unless the pair S_t, A_t appears in S_0, A_0, S_1, A_1, ..., S_{t-1}, A_{t-1}
            first_visit = True
            for step in episode_steps[:t]:  # first-visit!
                if S_t == step[0] and A_t == step[1]:
                    first_visit = False
                    break

            if not first_visit:
                continue

            # increment number of visits (n)
            number_of_visits[S_t][A_t] = number_of_visits[S_t][A_t] + 1

            # Q_{n+1} = Q_n + (R_n - Q_n) / n
            q_table[S_t][A_t] = q_table[S_t][A_t] + (G - q_table[S_t][A_t]) / number_of_visits[S_t][A_t]

        episode += 1

        right_actions_proption = quick_policy_check_q_table(q_table, True)
        right_actions_proptions.append(right_actions_proption)

        if right_actions_proption >= required_perfect_actions_propotion:
            break

    return right_actions_proptions


_, ax2 = plt.subplots()

if not os.path.exists('mc_learning_values_90.txt'):
    y = evaluate_epsilon_monte_carlo(0.90)
    with open('mc_learning_values90.txt', 'w') as f:
        for item in y:
            f.write("%s\n" % item)
else:
    y = [float(element) for element in tuple(open('mc_learning_values.txt', 'r'))]

y_mean = []
mean_number = 1000
for i in range(int(len(y) / mean_number)):
    if i * mean_number + mean_number > len(y):
        y_mean.append(np.mean(y[i * mean_number:]))
        break
    else:
        y_mean.append(np.mean(y[i * mean_number: i * mean_number + 10]))

y = y_mean
x = [i * mean_number for i in range(len(y))]

ax2.axhline(linewidth=1, color='darkred', y=0.95, ls='--')
ax2.yaxis.set_major_formatter(FuncFormatter(lambda l, _: '{:.0%}'.format(l)))
ax2.set_ylabel('Erreichte Konvergenz')
ax2.set_xlabel('Anzahl an benötigten Episoden')
ax2.plot(x, y, label='ɛ​ = 0,75')
ax2.set_yticks([0.95, *ax2.get_yticks()[1:-1]])

ax2.legend(loc='center', bbox_to_anchor=(0.5, -.30), shadow=False, ncol=1, frameon=False)
# plt.savefig('all_optimal.svg', format='svg', dpi=600, bbox_inches="tight")
plt.savefig('mc_hpt.svg', format='svg', dpi=600, bbox_inches="tight")
