import random
import math
import gym
import numpy as np

from rl_implementation.hyperparameter_tuning.quick_policy_check import quick_policy_check_q_table


def epsilon_greedy_action_selection(epsilon, q_table, state):
    if random.uniform(0, 1) < epsilon:
        # exploration
        return random.randint(0, len(q_table[state]) - 1)
    else:
        # exploitation
        action = np.argmax(q_table[state])

        # ties broken equally if more than one action satisfies condition
        all_max_indices = np.where(q_table[state] == q_table[state][action])[0]
        return np.random.choice(all_max_indices)


def evaluate_n_step_sarsa(epsilon, alpha=0.1, n=5, repetitions=10, required_perfect_actions_propotion=0.95,
                          max_number_of_episodes=10000, gamma=1):
    env = gym.make("Taxi-v2")  # Create environment
    # env._max_episode_steps = 1000
    action_size = env.action_space.n  # Number of possible actions
    state_size = env.observation_space.n  # Number of possible states

    number_of_episodes = []
    for _ in range(repetitions):
        q_table = np.zeros((state_size, action_size))
        # q_table = np.genfromtxt('./optimal_taxi_q_table', delimiter=',')

        episode = 0
        while True:
            # generate full episode
            episode_steps = []

            T = math.inf
            tau = 0
            t = 0

            # Initialize and store S =/= terminal
            current_state = env.reset()  # Reset the environment

            # Select and store an action
            current_action = epsilon_greedy_action_selection(epsilon, q_table, current_state)

            while tau < T - 1:
                if t < T:
                    # execute action
                    next_state, reward, done, info = env.step(current_action)
                    episode_steps.append((current_state, current_action, reward))

                    if done:
                        T = t + 1
                    else:
                        new_action = epsilon_greedy_action_selection(epsilon, q_table, next_state)

                tau = t - n + 1  # tau is the time whose estimate is being updated

                if tau >= 0:
                    G = 0
                    # +1 because sum sign (sigma) is inclusive and second param of range not!
                    for i in range(tau + 1, min(tau + n, T) + 1):
                        G = G + gamma ** (i - tau - 1) * episode_steps[i - 1][2]  # list is 0-indexed

                    if tau + n < T:
                        G = G + gamma ** n * q_table[episode_steps[tau + n - 1][0], episode_steps[tau + n - 1][1]]

                    old_q_value = q_table[episode_steps[tau - 1][0], episode_steps[tau - 1][1]]
                    q_table[episode_steps[tau - 1][0], episode_steps[tau - 1][1]] = \
                        old_q_value + alpha * (G - old_q_value)

                t = t + 1
                current_state = next_state
                current_action = new_action

            if quick_policy_check_q_table(q_table, False) > required_perfect_actions_propotion \
                    or episode > max_number_of_episodes:
                # print(str(epsilon) + ': ' + str(episode))
                number_of_episodes.append(episode)
                break

            # if episode % 1000 == 0:
            #     quick_policy_check_q_table(q_table)

            episode += 1

    print(str((epsilon, alpha, n)) + ': ' + str(np.mean(number_of_episodes)))
    return epsilon, alpha, n, np.mean(number_of_episodes)


if __name__ == '__main__':
    reps_10 = [(0.25, 0.05, 2, 7487.3),
               (0.25, 0.05, 3, 5032.4), (0.25, 0.05, 4, 4168.0), (0.25, 0.05, 5, 4687.2), (0.25, 0.05, 6, 6593.3),
               (0.25, 0.05, 7, 9266.4), (0.25, 0.05, 8, 10001.0), (0.25, 0.1, 2, 5394.1), (0.25, 0.1, 3, 4165.9),
               (0.25, 0.1, 4, 6896.7), (0.25, 0.1, 5, 9394.7), (0.25, 0.1, 6, 10001.0), (0.25, 0.1, 7, 10001.0),
               (0.25, 0.1, 8, 10001.0), (0.25, 0.15, 2, 4616.2), (0.25, 0.15, 3, 7072.5), (0.25, 0.15, 4, 10001.0),
               (0.25, 0.15, 5, 10001.0), (0.25, 0.15, 6, 10001.0), (0.25, 0.15, 7, 10001.0), (0.25, 0.15, 8, 10001.0),
               (0.3, 0.05, 2, 8053.6), (0.3, 0.05, 3, 5331.4), (0.3, 0.05, 4, 3828.4), (0.3, 0.05, 5, 3568.6),
               (0.3, 0.05, 6, 4039.5), (0.3, 0.05, 7, 5379.2), (0.3, 0.05, 8, 8354.5), (0.3, 0.1, 2, 4656.3),
               (0.3, 0.1, 3, 4100.6), (0.3, 0.1, 4, 4518.4), (0.3, 0.1, 5, 6233.9), (0.3, 0.1, 6, 8431.4),
               (0.3, 0.1, 7, 10001.0), (0.3, 0.1, 8, 9975.6), (0.3, 0.15, 2, 5371.1), (0.3, 0.15, 3, 4998.9),
               (0.3, 0.15, 4, 7812.3), (0.3, 0.15, 5, 10001.0), (0.3, 0.15, 6, 10001.0), (0.3, 0.15, 7, 10001.0),
               (0.3, 0.15, 8, 10001.0), (0.35, 0.05, 2, 9324.2), (0.35, 0.05, 3, 5839.5), (0.35, 0.05, 4, 3942.9),
               (0.35, 0.05, 5, 3582.0), (0.35, 0.05, 6, 3340.3), (0.35, 0.05, 7, 3247.3), (0.35, 0.05, 8, 4421.3),
               (0.35, 0.1, 2, 5380.1), (0.35, 0.1, 3, 4597.6), (0.35, 0.1, 4, 3933.4), (0.35, 0.1, 5, 4665.6),
               (0.35, 0.1, 6, 6170.7), (0.35, 0.1, 7, 8490.9), (0.35, 0.1, 8, 9538.0), (0.35, 0.15, 2, 5004.3),
               (0.35, 0.15, 3, 4912.4), (0.35, 0.15, 4, 6119.8), (0.35, 0.15, 5, 9487.0), (0.35, 0.15, 6, 10001.0),
               (0.35, 0.15, 7, 10001.0), (0.35, 0.15, 8, 10001.0), (0.4, 0.05, 2, 9912.0),
               (0.4, 0.05, 3, 6952.8), (0.4, 0.05, 4, 4717.5),
               (0.4, 0.05, 5, 3973.1), (0.4, 0.05, 6, 3168.4),
               (0.4, 0.05, 7, 3161.3), (0.4, 0.05, 8, 2775.1),
               (0.4, 0.1, 2, 5655.7), (0.4, 0.1, 3, 4350.4),
               (0.4, 0.1, 4, 3536.8), (0.4, 0.1, 5, 4261.5),
               (0.4, 0.1, 6, 3727.1), (0.4, 0.1, 7, 5110.7),
               (0.4, 0.1, 8, 7533.4), (0.4, 0.15, 2, 4990.4),
               (0.4, 0.15, 3, 4750.3), (0.4, 0.15, 4, 5334.0),
               (0.4, 0.15, 5, 7110.3), (0.4, 0.15, 6, 9391.8),
               (0.4, 0.15, 7, 9756.3), (0.4, 0.15, 8, 10001.0),
               (0.45, 0.05, 2, 10001.0), (0.45, 0.05, 3, 7482.1),
               (0.45, 0.05, 4, 5583.2), (0.45, 0.05, 5, 4378.3),
               (0.45, 0.05, 6, 3673.4), (0.45, 0.05, 7, 3294.4),
               (0.45, 0.05, 8, 2852.6), (0.45, 0.1, 2, 6945.1),
               (0.45, 0.1, 3, 4326.4), (0.45, 0.1, 4, 3906.7),
               (0.45, 0.1, 5, 3468.6), (0.45, 0.1, 6, 4164.4),
               (0.45, 0.1, 7, 4249.8), (0.45, 0.1, 8, 5110.6),
               (0.45, 0.15, 2, 5837.4), (0.45, 0.15, 3, 3957.0),
               (0.45, 0.15, 4, 4859.8), (0.45, 0.15, 5, 7529.2),
               (0.45, 0.15, 6, 9815.8), (0.45, 0.15, 7, 9247.6),
               (0.45, 0.15, 8, 10001.0), (0.5, 0.05, 2, 10001.0),
               (0.5, 0.05, 3, 9081.9), (0.5, 0.05, 4, 6555.8),
               (0.5, 0.05, 5, 5414.2), (0.5, 0.05, 6, 3968.4),
               (0.5, 0.05, 7, 3258.0), (0.5, 0.05, 8, 3041.3),
               (0.5, 0.1, 2, 7372.4), (0.5, 0.1, 3, 5231.8),
               (0.5, 0.1, 4, 4220.8), (0.5, 0.1, 5, 4226.9),
               (0.5, 0.1, 6, 3301.0), (0.5, 0.1, 7, 3591.9),
               (0.5, 0.1, 8, 3701.6), (0.5, 0.15, 2, 6274.9),
               (0.5, 0.15, 3, 5625.6), (0.5, 0.15, 4, 5874.0),
               (0.5, 0.15, 5, 6460.7), (0.5, 0.15, 6, 8092.7),
               (0.5, 0.15, 7, 10001.0), (0.5, 0.15, 8, 10001.0),
               (0.55, 0.05, 8, 3626.2),
                 (0.55, 0.1, 8, 5287.7), (0.55, 0.15, 8, 9935.4), (0.6, 0.05, 8, 4062.2),
                 (0.6, 0.1, 8, 4928.6), (0.6, 0.15, 8, 10001.0),
                 (0.65, 0.05, 8, 5390.6), (0.65, 0.1, 8, 7962.0),
                 (0.65, 0.15, 8, 10001.0), (0.7, 0.05, 8, 6657.6),
                 (0.7, 0.1, 8, 8451.3), (0.7, 0.15, 8, 10001.0),
                 (0.75, 0.05, 8, 7620.2), (0.75, 0.1, 8, 10001.0),
                 (0.75, 0.15, 8, 10001.0)]

    data = [(item[0], item[1], item[3]) for item in reps_10 if item[2] == 8]
    print(data)
