from operator import itemgetter

import matplotlib

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from numpy.random import randn
from scipy import array, newaxis

data = []

max_number_of_episodes = 1000

with open("tuning_results_double_q_learning", "r") as ins:
    index_counter = 0
    for line in ins:

        number_of_episodes = int(line.rsplit(' for alpha=')[0].split(' ')[-1])
        alpha = float(line.rsplit(' epsilon=')[0].split('=')[-1])
        epsilon = float(line.rsplit(' epsilon_decay=')[0].split('=')[-1])

        if number_of_episodes > max_number_of_episodes:
            number_of_episodes = max_number_of_episodes

        if number_of_episodes < 800:
            print((epsilon, alpha, number_of_episodes))
        if epsilon >= 0.5 and epsilon <= 0.9 and alpha >= 0.2:
            data.append((epsilon, alpha, number_of_episodes))

        index_counter += 1

data = np.array(data)

X = data[:, 0]
Y = data[:, 1]
Z = data[:, 2]

fig = plt.figure()
fig.set_dpi(200)
ax = fig.add_subplot(111, projection='3d')
surf = ax.plot_trisurf(X, Y, Z, cmap=cm.RdBu, linewidth=.5, antialiased=True, edgecolors='w')

ax.set_xlabel('Epsilon')
ax.set_ylabel('Alpha')

ax.zaxis.set_rotate_label(False)
ax.zaxis.label.set_rotation(90)
ax.set_zlabel('Anzahl an Episoden')

ax.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
ax.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
ax.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))

for spine in ax.spines.values():
    spine.set_visible(False)

fig.tight_layout()

ax.view_init(azim=335)
plt.savefig('double_q_learning_hpt.svg', format='svg', dpi=600, bbox_inches="tight")
