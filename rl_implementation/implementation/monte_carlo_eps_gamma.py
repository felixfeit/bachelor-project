import random
from multiprocessing.pool import Pool

import gym
import numpy as np

from rl_implementation.hyperparameter_tuning.quick_policy_check import quick_policy_check_q_table


def evaluate_epsilon_and_gamma_monte_carlo(epsilon, repetitions=2, required_perfect_actions_propotion=0.6,
                                           max_number_of_episodes=1000):
    env = gym.make("Taxi-v2")  # Create environment
    # env._max_episode_steps = 1000
    action_size = env.action_space.n   # Number of possible actions
    state_size = env.observation_space.n  # Number of possible states

    gamma_evaluations = []
    for gamma in np.arange(0.5, 1.01, 0.05):
        number_of_episodes = []
        for _ in range(repetitions):
            q_table = np.zeros((state_size, action_size))
            # q_table = np.genfromtxt('./optimal_taxi_q_table', delimiter=',')

            # incremental: store tuples with current avg and number of values (2D-array -> 500x6)
            number_of_visits = np.zeros((state_size, action_size))

            episode = 0
            while True:
                # generate full episode
                episode_steps = []
                state = env.reset()  # Reset the environment
                done = False

                while not done:
                    if random.uniform(0, 1) < epsilon:
                        # exploration
                        action = env.action_space.sample()
                    else:
                        # exploitation
                        action = np.argmax(q_table[state])

                        # ties broken equally if more than one action satisfies condition
                        all_max_indices = np.where(q_table[state] == q_table[state][action])[0]
                        action = np.random.choice(all_max_indices)

                    # execute action
                    new_state, reward, done, info = env.step(action)

                    # if done:
                    #     print(reward)

                    # (S_t, A_t, R_{t+1})
                    episode_steps.append((state, action, reward))

                    state = new_state

                # if len(episode_steps) == env._max_episode_steps:
                #     continue

                # calculate new averages and adjust policy
                G = 0

                for t in reversed(range(len(episode_steps))):
                    S_t, A_t, R_t_plus_1 = episode_steps[t]
                    G = gamma * G + R_t_plus_1

                    # unless the pair S_t, A_t appears in S_0, A_0, S_1, A_1, ..., S_{t-1}, A_{t-1}
                    first_visit = True
                    for step in episode_steps[:t]:  # first-visit!
                        if S_t == step[0] and A_t == step[1]:
                            first_visit = False
                            break

                    if not first_visit:
                        continue

                    # increment number of visits (n)
                    number_of_visits[S_t][A_t] = number_of_visits[S_t][A_t] + 1

                    # Q_{n+1} = Q_n + (R_n - Q_n) / n
                    q_table[S_t][A_t] = q_table[S_t][A_t] + (G - q_table[S_t][A_t]) / number_of_visits[S_t][A_t]

                episode += 1

                if episode % 3 == 0 and quick_policy_check_q_table(q_table) > required_perfect_actions_propotion:
                    # print(str(epsilon) + ': ' + str(episode))
                    number_of_episodes.append(episode)
                    break

                if episode > max_number_of_episodes:
                    number_of_episodes.append(max_number_of_episodes)
                    break

                # decay epsilon
                # epsilon = (epsilon * epsilon_decay) if epsilon > 0.1 else 0.1

        gamma_evaluations.append((gamma, np.mean(number_of_episodes)))

    print(gamma_evaluations)
    return epsilon, gamma_evaluations


if __name__ == '__main__':
    p = Pool()
    task_results = p.map(evaluate_epsilon_and_gamma_monte_carlo, np.arange(0.7, 1.01, 0.05))
    p.close()

    all_means = {}
    for epsilon, gamma_evaluations in task_results:
        all_means['%.2f' % epsilon] = {}
        for gamma, mean in gamma_evaluations:
            all_means['%.2f' % epsilon]['%.2f' % gamma] = mean

    print(all_means)

# gamma == 1:
# {
#   '0.05': 41111.9,
# 	'0.10': 12466.2,
# 	'0.15': 5234.7,
# 	'0.20': 3665.7,
# 	'0.25': 2221.2,
# 	'0.30': 1906.2,
#   '0.35': 1431.3,
# 	'0.40': 1398.3,
# 	'0.45': 1139.16,
# 	'0.50': 988.8,
# 	'0.55': 980.04,
# 	'0.60': 850.92,
#   '0.65': 877.98,
# 	'0.70': 884.4,
# 	'0.75': 886.02,
# 	'0.80': 1078.5,
#   '0.85': 1470.54,
# 	'0.90': 3138.0,
# 	'0.95': 25040.7
#  }

# epsilon -> gamma -> average episodes
# {
#     '0.10': {
#         '0.10': 1000.0,
#         '0.20': 1000.0,
#         '0.30': 1000.0,
#         '0.40': 1000.0,
#         '0.50': 1000.0,
#         '0.60': 1000.0,
#         '0.70': 1000.0,
#         '0.80': 1000.0,
#         '0.90': 1000.0
#     },
#     '0.20': {
#         '0.10': 1000.0,
#         '0.20': 1000.0,
#         '0.30': 1000.0,
#         '0.40': 1000.0,
#         '0.50': 1000.0,
#         '0.60': 1000.0,
#         '0.70': 1000.0,
#         '0.80': 1000.0,
#         '0.90': 1000.0
#     },
#     '0.30': {
#         '0.10': 1000.0,
#         '0.20': 1000.0,
#         '0.30': 1000.0,
#         '0.40': 1000.0,
#         '0.50': 1000.0,
#         '0.60': 1000.0,
#         '0.70': 1000.0,
#         '0.80': 1000.0,
#         '0.90': 720.5
#     },
#     '0.40': {
#         '0.10': 1000.0,
#         '0.20': 1000.0,
#         '0.30': 1000.0,
#         '0.40': 1000.0,
#         '0.50': 1000.0,
#         '0.60': 1000.0,
#         '0.70': 1000.0,
#         '0.80': 784.5,
#         '0.90': 818.0
#     },
#     '0.50': {
#         '0.10': 1000.0,
#         '0.20': 1000.0,
#         '0.30': 1000.0,
#         '0.40': 1000.0,
#         '0.50': 1000.0,
#         '0.60': 1000.0,
#         '0.70': 1000.0,
#         '0.80': 1000.0,
#         '0.90': 540.0
#     },
#     '0.60': {
#         '0.10': 1000.0,
#         '0.20': 1000.0,
#         '0.30': 1000.0,
#         '0.40': 1000.0,
#         '0.50': 1000.0,
#         '0.60': 1000.0,
#         '0.70': 1000.0,
#         '0.80': 742.5,
#         '0.90': 517.5
#     },
#     '0.70': {
#         '0.10': 1000.0,
#         '0.20': 1000.0,
#         '0.30': 1000.0,
#         '0.40': 1000.0,
#         '0.50': 1000.0,
#         '0.60': 1000.0,
#         '0.70': 1000.0,
#         '0.80': 740.0,
#         '0.90': 405.0
#     },
#     '0.80': {
#         '0.10': 1000.0,
#         '0.20': 1000.0,
#         '0.30': 1000.0,
#         '0.40': 1000.0,
#         '0.50': 1000.0,
#         '0.60': 1000.0,
#         '0.70': 690.5,
#         '0.80': 681.0,
#         '0.90': 436.5
#     }
# }

# {
#     '0.70': {
#         '0.50': 1000.0,
#         '0.55': 1000.0,
#         '0.60': 798.5,
#         '0.65': 1000.0,
#         '0.70': 1000.0,
#         '0.75': 950.0,
#         '0.80': 1000.0,
#         '0.85': 369.0,
#         '0.90': 385.5,
#         '0.95': 375.0,
#         '1.00': 450.0
#     },
#     '0.75': {
#         '0.50': 1000.0,
#         '0.55': 1000.0,
#         '0.60': 1000.0,
#         '0.65': 1000.0,
#         '0.70': 752.0,
#         '0.75': 1000.0,
#         '0.80': 738.5,
#         '0.85': 465.0,
#         '0.90': 483.0,
#         '0.95': 423.0,
#         '1.00': 532.5
#     },
#     '0.80': {
#         '0.50': 1000.0,
#         '0.55': 1000.0,
#         '0.60': 1000.0,
#         '0.65': 642.5,
#         '0.70': 1000.0,
#         '0.75': 1000.0,
#         '0.80': 822.0,
#         '0.85': 734.0,
#         '0.90': 439.5,
#         '0.95': 441.0,
#         '1.00': 706.5
#     },
#     '0.85': {
#         '0.50': 1000.0,
#         '0.55': 1000.0,
#         '0.60': 1000.0,
#         '0.65': 1000.0,
#         '0.70': 1000.0,
#         '0.75': 1000.0,
#         '0.80': 1000.0,
#         '0.85': 489.0,
#         '0.90': 556.5,
#         '0.95': 408.0,
#         '1.00': 905.0
#     },
#     '0.90': {
#         '0.50': 1000.0,
#         '0.55': 1000.0,
#         '0.60': 1000.0,
#         '0.65': 1000.0,
#         '0.70': 1000.0,
#         '0.75': 1000.0,
#         '0.80': 1000.0,
#         '0.85': 1000.0,
#         '0.90': 624.0,
#         '0.95': 513.0,
#         '1.00': 1000.0
#     },
#     '0.95': {
#         '0.50': 1000.0,
#         '0.55': 1000.0,
#         '0.60': 1000.0,
#         '0.65': 1000.0,
#         '0.70': 1000.0,
#         '0.75': 1000.0,
#         '0.80': 1000.0,
#         '0.85': 669.5,
#         '0.90': 784.5,
#         '0.95': 935.0,
#         '1.00': 1000.0
#     },
#     '1.00': {
#         '0.50': 1000.0,
#         '0.55': 1000.0,
#         '0.60': 1000.0,
#         '0.65': 1000.0,
#         '0.70': 1000.0,
#         '0.75': 1000.0,
#         '0.80': 1000.0,
#         '0.85': 1000.0,
#         '0.90': 1000.0,
#         '0.95': 972.5,
#         '1.00': 1000.0
#     }
# }
