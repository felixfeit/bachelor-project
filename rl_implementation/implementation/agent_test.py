import logging
import os
import sys

import gym
import numpy as np


class AgentTest:
    def __init__(self, environment_name, episodes, q_table_file_name, logger=None):
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger(__name__)

        self.q_table_file_name = q_table_file_name
        self.environment = gym.make(environment_name)

        if self.q_table_file_name and os.path.isfile(self.q_table_file_name):
            self.q_table = np.genfromtxt(self.q_table_file_name, delimiter=',')
            if self.q_table.shape != (self.environment.observation_space.n, self.environment.action_space.n):
                self.logger.error('Dimensions of given q-table do not match.')
                sys.exit(1)

        for i in range(episodes):
            print('Run episode %i, finished after %i timesteps.' % (i, self._episode()))

        sys.exit(0)

    def _episode(self):
        state = self.environment.reset()
        self.environment.render()
        done = False

        time_steps = 0
        while not done:

            print('')
            action = np.argmax(self.q_table[state])
            print('selected action: ' + str(action))
            state, reward, done, info = self.environment.step(action)
            print('received reward: ' + str(reward))
            self.environment.render()

            # time.sleep(2)
            input()

            if done:
                break

            time_steps += 1

        return time_steps


if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    # agent_test = AgentTest('FrozenLake-v0', 1, './q_tables/q_table_frozen_lake.csv', logger=logger)
    agent_test = AgentTest('Taxi-v2', 1, './q_tables/q_table_taxi_optimal.csv', logger=logger)
